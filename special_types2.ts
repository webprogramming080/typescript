let w: unknown = 1;
w = "string";
w = {
    runANonxistentMethod: () => {
        console.log("I think therefore I am");
    }
} as { runANonxistentMethod: () => void }

if (typeof w === 'object' && w !== null) {
    (w as { runANonxistentMethod: Function }).runANonxistentMethod();
}